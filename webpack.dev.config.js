const merge = require('webpack-merge');
const baseConfig = require('./src/lib/webpack/webpack-config');
const devConfig = require('./src/lib/webpack/webpack-dev-config');
const devServerConfig = require('./src/lib/webpack/webpack-dev-server-config');

module.exports = merge(
  baseConfig,
  devConfig,
  devServerConfig
);
