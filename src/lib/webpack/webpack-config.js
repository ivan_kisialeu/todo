const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const { CheckerPlugin  } = require('awesome-typescript-loader');

module.exports = {
  entry: path.resolve('src', './index.jsx'),
  output: {
    path: path.resolve('dist'),
    filename: '[name].js',
    publicPath: '/',
  },
  resolve: {
    modules: [
      path.resolve('node_modules'),
      path.resolve('src'),
    ],
    extensions: [
      '.js',
      '.jsx',
    ],
    alias: {
      components: path.resolve('src', 'components'),
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
      {
        enforce: 'pre',
        test: /\.(ts|tsx)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: 'public/index.html',
      inject: 'body',
    }),
    new CheckerPlugin(),
  ],
};
