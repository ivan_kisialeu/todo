const path = require('path');

module.exports = {
  devServer: {
    contentBase: path.resolve('public'),
    compress: true,
    port: 3000,
    historyApiFallback: true,
  },
};
