import {
  TODO_LIST
} from '../const';

class TodoService {

  constructor() {
    this.todoList = this.loadTodoList();
  }

  addTodoItem(todoItem) {

    this.todoList.push({
      id: this.todoList.length + 1,
      ...todoItem
    });
    this.saveTodoList();
  }

  editTodoItem(id, {
    title,
    description
  }) {
    const todoItem = this.getTodoItem(id);

    todoItem.title = title;
    todoItem.description = description;
    this.saveTodoList();
  }

  removeTodoItem(id) {
    this.todoList = this.todoList.filter((todoItem) => todoItem.id !== id);
    this.saveTodoList();
  }

  resolveTodoItem(id) {
    const todoItem = this.getTodoItem(id);

    todoItem.status = 'resolved';
    this.saveTodoList();
  }

  getTodoItem(id) {
    const task = this.todoList.find((task) => task.id == id);

    if (!task) {
      throw new Error('Task not found');
    }

    return task;
  }

  saveTodoList() {
    sessionStorage.setItem(TODO_LIST, JSON.stringify(this.todoList));
  }

  loadTodoList() {
    const todoList = JSON.parse(sessionStorage.getItem(TODO_LIST)) || [];

    return todoList.sort((a, b) => {
      const titleA = a.title.toUpperCase();
      const titleB = b.title.toUpperCase();

      if (titleA > titleB) {
        return 1;
      }

      if (titleA < titleB) {
        return -1;
      }

      return 0;
    })
  }
};

export const todoService = new TodoService();