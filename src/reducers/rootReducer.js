import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { todoListReducer } from './todoListReducer';

export const rootReducer = history => combineReducers({
  todoList: todoListReducer,
  router: connectRouter(history)
});
