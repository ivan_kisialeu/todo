import { REQUEST_TODO_LIST_SUCCESS } from '../views/HomePage/const';
import { REQUEST_TODO_ITEM_SUCCESS } from '../views/TodoItemPage/const';

const initialState = {
  todoList: [],
  todoItem: {},
};

export const todoListReducer = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case REQUEST_TODO_LIST_SUCCESS:
      return { ...state, todoList: action.todoList};
    case REQUEST_TODO_ITEM_SUCCESS:
      return { ...state, todoItem: action.todoItem};
    default:
      return state;
  }
};
