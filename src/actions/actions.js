import { ROUTING } from '../const';

export const redirectOnPage = url => ({
  type: ROUTING,
  payload: {
    url,
    method: 'push'
  }
});
