import React from 'react';
import PropType from 'prop-types';
import { ContolledInput } from '..';

export const TodoManagementForm = ({ title, description, todoManagementHandler }) => (
  <form onSubmit={todoManagementHandler}>
    <div>
      <ContolledInput.Input label='Title:' value={title} name='title' />
    </div>
    <div>
      <ContolledInput.TextArea label='Description' value={description} name='description' />
    </div>
    <button type="submit">Save</button>
  </form>
);

TodoManagementForm.propTypes = {
  title: PropType.string,
  description: PropType.string,
}
