import React, { useState } from 'react';

export const withControl = WrappedComponent => ({ value, ...props }) => {
  const [inputValue, setInputValue] = useState(); 

  const onChangeHandler = (e) => setInputValue(e.target.value);

  return <WrappedComponent onChange={onChangeHandler} value={inputValue || value} {...props} />
};
