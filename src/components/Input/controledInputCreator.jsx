import { withControl } from './withControl';
import { Input } from './Input';
import { TextArea } from './TextArea';

export const ContolledInput = {
  Input: withControl(Input),
  TextArea: withControl(TextArea),
}