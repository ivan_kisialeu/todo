import React from 'react';
import PropType from 'prop-types';

export const TextArea = ({ label, onChange, ...rest }) => (
  <>
    {label && <label>{label}</label>}
    <textarea {...rest} onChange={onChange} />
  </>
);

TextArea.propTypes = {
  label: PropType.string,
  value: PropType.string,
  name: PropType.string,
  onChange: PropType.func,
};
