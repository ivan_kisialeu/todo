import React from 'react';
import PropType from 'prop-types';

export const Input = ({ label, onChange, ...rest }) => (
  <>
    {label && <label>{label}</label>}
    <input {...rest} onChange={onChange} />
  </>
);

Input.propTypes = {
  label: PropType.string,
  value: PropType.string,
  name: PropType.string,
  onChange: PropType.func,
};

