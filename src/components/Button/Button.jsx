import React from 'react';
import PropType  from 'prop-types';

export const Button = ({ onClick, value, type }) => (
  <button onClick={onClick} type={type}>{value}</button>
);

Button.propTypes = {
  onClick: PropType.func,
  type: PropType.string,
  value: PropType.string.isRequired,
};
