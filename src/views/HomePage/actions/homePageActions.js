import { todoService } from '../../../services/todoService';
import {
  REQUEST_TODO_LIST,
  REQUEST_TODO_LIST_SUCCESS,
  REQUEST_ADD_TODO,
  REQUEST_ADD_TODO_SUCCESS,
  REQUEST_REMOVE_TODO,
  REQUEST_REMOVE_TODO_SUCCESS,
  REQUEST_RESOLVE_TODO,
  REQUEST_RESOLVE_TODO_SUCCESS,
} from '../const';

export const loadTodoList = () => (dispatch) => {
  dispatch({ type: REQUEST_TODO_LIST })

  const todoList = todoService.loadTodoList();

  dispatch({
    type: REQUEST_TODO_LIST_SUCCESS,
    todoList,
  });
}

export const addTodoItem = (item) => (dispatch) => {
  dispatch({ type: REQUEST_ADD_TODO })

  todoService.addTodoItem(item);

  dispatch({ type: REQUEST_ADD_TODO_SUCCESS });

  dispatch(loadTodoList());
}

export const removeTodoItem = (id) => (dispatch) => {
  dispatch({ type: REQUEST_REMOVE_TODO })

  todoService.removeTodoItem(id);

  dispatch({ type: REQUEST_REMOVE_TODO_SUCCESS });

  dispatch(loadTodoList());
}

export const resolveTodoItem = (id) => (dispatch) => {
  dispatch({ type: REQUEST_RESOLVE_TODO })

  todoService.resolveTodoItem(id);

  dispatch({ type: REQUEST_RESOLVE_TODO_SUCCESS });

  dispatch(loadTodoList());
}
