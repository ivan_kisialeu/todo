import React from 'react';
import PropType from 'prop-types';
import { Button } from '../../../components';

export const TodoList = ({ todoList, removeTodoItem, resolveTodoItem, selectItemHandler }) => (
  <ul>
    {todoList.map(({ id, title, status }) => (
      <li key={`${id}`}>
        <span onClick={() => selectItemHandler(id)}>{title}</span>

        <Button onClick={() => removeTodoItem(id)} value='Remove' />
        {status !== 'resolved' && <Button onClick={() => resolveTodoItem(id)} value='Resolve' />}
      </li>
    ))}
  </ul>
);

TodoList.defaultProps = {
  todoList: [],
};

TodoList.propTypes = {
  todoList: PropType.array.isRequired,
  removeTodoItem: PropType.func,
  resolveTodoItem: PropType.func,
  selectItemHandler: PropType.func,
};
