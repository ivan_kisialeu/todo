import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { TodoManagementForm } from '../../../components';
import { TodoList } from '../components/TodoList';
import { redirectOnPage } from '../../../actions';
import {
  loadTodoList,
  addTodoItem,
  removeTodoItem,
  resolveTodoItem,
} from '../actions';

export const HomePageBase = ({
  todoList,
  loadTodoList,
  addTodoItem,
  removeTodoItem,
  resolveTodoItem,
  redirectOnPage,
}) => {
  useEffect(() => loadTodoList(), [loadTodoList]);

  const todoManagementHandler = (e) => {
    e.preventDefault();
    addTodoItem({
      title: e.target.title.value,
      description: e.target.description.value,
    });
  }

  const selectItemHandler = (id) => redirectOnPage(`/todo/${id}`);

  return (
    <>
      <TodoList
        todoList={todoList}
        selectItemHandler={selectItemHandler}
        removeTodoItem={removeTodoItem}
        resolveTodoItem={resolveTodoItem}
      />
      <TodoManagementForm todoManagementHandler={todoManagementHandler} />
    </>
  );
};

const mapStateToProps = ({ todoList }) => ({
  todoList: todoList.todoList,
  todoItem: todoList.todoItem,
});

const mapDispatchToProps = {
  loadTodoList,
  addTodoItem,
  removeTodoItem,
  resolveTodoItem,
  redirectOnPage,
};

export const HomePage = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePageBase);
