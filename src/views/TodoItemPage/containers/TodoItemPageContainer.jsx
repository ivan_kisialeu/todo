import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
  loadTodoItem,
  editTodoItem,
} from '../actions';
import { TodoManagementForm } from '../../../components';

export const TodoItemPageBase = ({
  todoItem,
  loadTodoItem,
  editTodoItem,
  match,
}) => {
  useEffect(() => loadTodoItem(match.params.id), [loadTodoItem]);

  const todoManagementHandler = (e) => {
    e.preventDefault();
    editTodoItem(match.params.id, {
      title: e.target.title.value,
      description: e.target.description.value,
    });
  }

  return (
    <TodoManagementForm {...todoItem} todoManagementHandler={todoManagementHandler} />
  );
};

const mapStateToProps = ({ todoList }) => ({
  todoItem: todoList.todoItem,
});

const mapDispatchToProps = {
  loadTodoItem,
  editTodoItem,
};

export const TodoItemPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoItemPageBase);
