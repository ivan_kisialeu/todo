import { todoService } from '../../../services/todoService';
import {
  REQUEST_TODO_ITEM,
  REQUEST_TODO_ITEM_SUCCESS,
  REQUEST_EDIT_TODO,
  REQUEST_EDIT_TODO_SUCCESS,
} from '../const';
import { redirectOnPage } from '../../../actions';
import { HOME_PAGE_URL } from '../../../router/routes';

export const loadTodoItem = (id) => (dispatch) => {
  dispatch({ type: REQUEST_TODO_ITEM })

  const todoItem = todoService.getTodoItem(id);

  dispatch({
    type: REQUEST_TODO_ITEM_SUCCESS,
    todoItem,
  });
}

export const editTodoItem = (id, item) => dispatch => {
  dispatch({ type: REQUEST_EDIT_TODO })

  todoService.editTodoItem(id, item);

  dispatch({ type: REQUEST_EDIT_TODO_SUCCESS });

  dispatch(redirectOnPage(HOME_PAGE_URL));
}
