import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { 
  HomePage,
  TodoItemPage,
} from '../views';
import {
  HOME_PAGE_URL,
  TODO_ITEM_PAGE_URL,
} from './routes';

export const Router = () => (
  <Switch>
    <Route exact path={HOME_PAGE_URL} component={HomePage} />
    <Route exact path={TODO_ITEM_PAGE_URL} component={TodoItemPage} />
  </Switch>
);
