const merge = require('webpack-merge');
const baseConfig = require('./src/lib/webpack/webpack-config');
const prodConfig = require('./src/lib/webpack/webpack-prod-config');

module.exports = merge(
  baseConfig,
  prodConfig
);
